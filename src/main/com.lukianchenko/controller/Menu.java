package controller;

import fibonachi.FibonachiArray;
import fibonachi.FibonachiPrinter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * This class initiates menu of Fibonacci program
 * (here is all control logic )
 * @author Yuriy Lukianchenko qlukianchenkop@gmail.com
 * @version 1.0
 * @since 2019-04-01
 */
public class Menu {
    private int option;
    private int Nmin;
    private int Nmax;
    private FibonachiArray fibArray;
    private FibonachiPrinter fibPrinter = new FibonachiPrinter();
    public void start() throws IOException {
        System.out.println("Hello, You are using program 'Fibonacci' ");
        System.out.println("before we start please enter the bounds of " +
                "Fibonacci set (zero element = 0, first element = 1),");
        this.inputSetBoundaries();
        fibArray = new FibonachiArray();
        fibArray.buildFibonachiArray(Nmin, Nmax);
        this.showInfoforUser();
        this.execute();
    }

    /**
     * Read option what user want to do.
     * @return chosen option by user
     * @throws IOException when user enter incorrect option
     */
    private int read() throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String option = br.readLine();
        return Integer.parseInt(option);
    }

    /**
     * Execute one of actions implemented in FibonachiPrinter and described in
     * showInfoforUser method.
     * @throws IOException when user enter incorrect option
     */
    private void execute() throws IOException {
        finish:
        for(;;){
            try {
                option = this.read();
            }
            catch (IOException e ) {
                System.out.println("incorrect input");
            }
            switch(option){
                case 1:
                    fibPrinter.printOddEvenNumbers(fibArray.fibonachiArray);
                    this.showInformationAfterPerforming();
                    break;
                case 2:
                    fibPrinter.printSumOddEvenNumbers(fibArray.fibonachiArray);
                    this.showInformationAfterPerforming();
                    break;
                case 3:
                    fibPrinter.printPercentageOfOddEvenNumbers(fibArray.fibonachiArray);
                    this.showInformationAfterPerforming();
                    break;
                case 4:
                    fibPrinter.printBiggestOddBigestEvenNumbers(fibArray.fibonachiArray);
                    this.showInformationAfterPerforming();
                    break;
                case 5:
                    this.inputSetBoundaries();
                    this.showInformationAfterPerforming();
                    break;
                case 6:
                    this.showInfoforUser();
                    this.showInformationAfterPerforming();
                    break;
                case 7:
                    break finish;
                default:
                    System.out.println("unexceptable choice");
            }
        }
    }

    /**
     * Show list of actions which user can choose.
     */
    private void showInfoforUser(){
        System.out.println("Program can performs next actions:");
        System.out.println("-----------------------------------------------------------------------------------");
        System.out.println("| 1 - Print odd and even number of calculated Fibonacci array                      |");
        System.out.println("|     (odd numbers in increasing order and even numbers in decreasing order).      |");
        System.out.println("| 2 - Print sum of odd numbers and sum of even numbers.                            |");
        System.out.println("| 3 - Print percentage of odd numbers and percentage of even numbers.              |");
        System.out.println("| 4 - Input new maximal element of Fibonacci set and print the biggest odd number  |");
        System.out.println("|     and the biggest odd number.                                                  |");
        System.out.println("| 5 - reenter boundaries of Fibonacci set.                                         |");
        System.out.println("| 6 - show all options again.                                                      |");
        System.out.println("| 7 - quit Fibonacci program.                                                      |");
        System.out.println("-----------------------------------------------------------------------------------");
        System.out.println("what do you want to do ? enter number of option:");

    }

    /**
     * Show information after action has been performed.
     */
    private void showInformationAfterPerforming(){
        System.out.println("Action have been performed, chose option (show list of options enter 6)");
    }

    /**
     *  Set minimal and maximal boundaries of Fibonacci array which user has chosen.
     */
    private void inputSetBoundaries(){
        System.out.println("enter minimal element of Fibonacci set:");
        try {
            Nmin = this.read();
        }
        catch (IOException e ) {
            System.out.println("incorrect input");
        }
        System.out.println("enter maximal element of Fibonacci set");
        try {
            Nmax = this.read();
        }
        catch (IOException e ) {
            System.out.println("incorrect input");
        }
    }
}
