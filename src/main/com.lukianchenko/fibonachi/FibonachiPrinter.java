package fibonachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class performs actions with numbers of Fibonacci set.
 * @author Yuriy Lukianchenko qlukianchenkop@gmail.com
 * @version 1.0
 * @since 2019-04-01
 */
public class FibonachiPrinter {
    /**
     * Print odd and even numbers of
     * Fibonacci array (odd number in increasing order,
     * even numbers in decreasing order).
     * @param fiArr Fibonacci array
     */
    public void printOddEvenNumbers(int[] fiArr){
        System.out.println("odd numbers:");
        for(int i: fiArr){
            if ((i % 2) == 1){
                System.out.println(i);
            }
        }
        System.out.println("even numbers:");
        for(int i = fiArr.length -1; i >= 0; i-- ){
            if ((i % 2) == 0){
                System.out.println(i);
            }
        }
    }

    /**
     * Print the biggest odd and the biggest even number of Fibonacci array.
     * @param fiArr Fibonacci array
     * @throws IOException when user enter incorrect option
     */
    public void printBiggestOddBigestEvenNumbers(int[] fiArr) throws IOException{
        int biggestOdd;
        int biggestEven;
        FibonachiArray fiboArray = new FibonachiArray();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Please enter number of maximal element of Fibonacci set");
        String Nmax = br.readLine();
        fiboArray.buildFibonachiArray(0,Integer.parseInt(Nmax));
        int lastElement = fiboArray.fibonachiArray[fiboArray.fibonachiArray.length - 1];
        int preLastElement = fiboArray.fibonachiArray[fiboArray.fibonachiArray.length - 2];
        int prePreLastElement = fiboArray.fibonachiArray[fiboArray.fibonachiArray.length - 3];



        if ((lastElement % 2) == 0){
            biggestEven = lastElement;
            biggestOdd = preLastElement;
        } else {
            biggestOdd = lastElement;
            if ((preLastElement % 2) == 0) {
                biggestEven = preLastElement;
            } else {
                biggestEven = prePreLastElement;
            }
        }
        System.out.println("biggest odd number = " + biggestOdd);
        System.out.println("biggest even number = " + biggestEven);
    }

    /**
     * Print percentage of odd and even numbers in Fibonacci array.
     * @param fiArr Fibonacci array
     */
    public void printPercentageOfOddEvenNumbers(int[] fiArr){
        int fiboQuantityOdd = 0;
        int fiboQuantityEven = 0;
        float percentageOfOddNumbers = 0;
        float percentageOfEvenNumbers = 0;
        for(int i: fiArr){
            if ((i % 2) == 1){
                fiboQuantityOdd = fiboQuantityOdd + 1;
            }
            if ((i % 2) == 0){
                fiboQuantityEven = fiboQuantityEven + 1;
            }
        }
        percentageOfOddNumbers = fiboQuantityOdd / (float)(fiboQuantityOdd + fiboQuantityEven ) * 100;
        percentageOfEvenNumbers = fiboQuantityEven / (float)(fiboQuantityOdd + fiboQuantityEven ) * 100;
        System.out.println("percentage of odd numbers = " + percentageOfOddNumbers + " %");
        System.out.println("percentage of even numbers = " + percentageOfEvenNumbers + " %");
    }

    /**
     * Print sum of odd numbers and sum of even numbers of Fibonacci array.
     * @param fiArr Fibonacci array
     */
    public void printSumOddEvenNumbers(int[] fiArr){
        int fiboSumOdd = 0;
        int fiboSumEven = 0;
        for(int i: fiArr){
            if ((i % 2) == 1){
                fiboSumOdd = fiboSumOdd + i;
            }
            if ((i % 2) == 0){
                fiboSumEven = fiboSumEven + i;
            }
        }
        System.out.println("Summ of odd numbers = " + fiboSumOdd);
        System.out.println("Summ of even numbers = " + fiboSumEven);
    }
}
