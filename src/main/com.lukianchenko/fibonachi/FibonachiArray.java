package fibonachi;
/**
 * This class work with Fibonacci set.
 * @author Yuriy Lukianchenko qlukianchenkop@gmail.com
 * @version 1.0
 * @since 2019-04-01
 */
public class FibonachiArray {
    /**
     * contains built Fibonacci array
     */
    public int[] fibonachiArray;
    /**
     * contains additional Fibonacci array
     */
    private int[] preFibonachiArray;

    /**
     * build Fibonacci array,
     * create an instance of Fibonacci array
     * @param min minimal element of Fibonacci series
     * @param max maximal element of Fibonacci series
     */
    public void buildFibonachiArray(int min, int max){
        fibonachiArray = new int[max - min + 1];
        preFibonachiArray = new int[max+1];
        preFibonachiArray[0] = 0;
        preFibonachiArray[1] = 1;
        for(int i = 2; i < max+1; i++){
            preFibonachiArray[i] = preFibonachiArray[i-1] + preFibonachiArray[i-2];
        }
        for(int j = 0; j < max - min + 1; j++){
            fibonachiArray[j] = preFibonachiArray[min + j];
        }
    }
}
