import java.io.IOException;
import controller.*;

/**
 * This class initiates Fibonacci program
 * @author Yuriy Lukianchenko qlukianchenkop@gmail.com
 * @version 1.0
 * @since 2019-04-01
 */
public class Start {
    /**
     * This the main method start the program
     * @param args
     * @throws IOException when user enter incorrect option
     */
    public static void main(String[] args) throws IOException{
        Menu menu = new Menu();
        menu.start();
    }
}
